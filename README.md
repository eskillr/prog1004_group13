# PROG1004_Group13

```
Contact information for each group member
```

Eskil Lykke Refsgaard  
    - mail: eskillr@stud.ntnu.no  
    - tlf: +47 95331215  

Andreas Lindstøl  
    - mail: andlinds@stud.ntnu.no  
    - tlf:  +47 94837019  

Andreas Haugen  
    - mail: andrea11@stud.ntnu.no  
    - tlf: +47 98675317  

Kevin Haug  
    - mail: kevinha@stud.ntnu.no  
    - tlf: +47 98041577  

Stefan Moehrdel  
    - mail: stefamoe@stud.ntnu.no  
    - tlf: +47 99112161
